# Python base image
FROM python:3.11-slim

# Creating Working Directory inside the container
WORKDIR /app

#Copying files to working directory
COPY . /app

#Installing requirements.txt file
RUN apt-get update && pip install --no-cache-dir -r requirements.txt

CMD ["sh", "-c", "python manage.py migrate && python manage.py runserver 0.0.0.0:8000"]
